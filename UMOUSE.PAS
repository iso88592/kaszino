                  (****************************************)
                  (** UMouse.pas         V�rkonyi Tibor  **)
                  (**                                    **)
                  (*************  2004.02.12  *************)
                  (****************************************)
                  (*****)UNIT               Umouse;(*******)
                  (****************************************)
                                 INTERFACE
Uses dos,TGraph;
Type
      Tmouseshape=array[0..15,0..15]of byte;

      Tmouse=record
      X,Y:word;
      Gombok:word;
      lhx,lhy:word;
      katt:boolean;
      kattido:word;
      duplakatt:boolean;
      End;
var
r:registers;
mouse,mousebuff:Tmouseshape;{eg�r alatti ter�let}
mouseoldx,mouseoldy:word;{az eg�r "r�gi" koordin�t�i}
t:tmouse;

Procedure GetMouse(var T:Tmouse);
Procedure putmouse;
procedure initmouse;
procedure freshmouse;
Procedure ClearDevice;
function installusershape(s:string):integer;
                               IMPLEMENTATION
function installusershape(s:string):integer;
var f:file;
begin
assign(f,s);
{$I-}
reset(f,1);
{$I+}
if ioresult=0 then begin
blockread(f,mouse,sizeof(mouse));
close(f);
installusershape:=0;
end
else installusershape:=-1;
end;
procedure getmouse(var T:Tmouse);
var x,y:word;
begin
r.ax:=3;intr($33,r);
t.x:=r.cx div 4;
t.y:=r.dx div 4;
if t.kattido>0 then dec(t.kattido);
if (r.bx and 1=1)and(not t.katt) then begin t.katt:=true;t.lhx:=t.x;t.lhy:=t.y;end;
if (r.bx and 1=1)and(t.katt)and(t.kattido>0)then t.duplakatt:=true;
if (r.bx and 1=0){or(abs(t.lhx-t.x)>3)or(abs(t.lhy-t.y)>3)} then t.duplakatt:=false;
if(t.katt)and(r.bx and 1=0)then begin t.katt:=false;t.kattido:=400;end;
t.gombok:=r.bx;
end;
Procedure putmouse;
var x,y:word;
begin
mouseoldx:=t.x;mouseoldy:=t.y;
getmouse(t);
if (mouseoldx<>t.x)or(mouseoldy<>t.y)then begin
for x:=0 to 15 do for y:=0 to 15 do putpixel(mouseoldx+x,mouseoldy+y,mousebuff[x,y]);
for x:=0 to 15 do for y:=0 to 15 do mousebuff[x,y]:=getpixel(t.x+x,t.y+y);
for x:=0 to 15 do for y:=0 to 15 do if mouse[y,x]<>0 then if mouse[y,x]=16 then putpixel(t.x+x,t.y+y,0)
else putpixel(t.x+x,t.y+y,mouse[y,x]);
end;
end;
procedure freshmouse;
var x,y:word;
begin
for x:=0 to 15 do for y:=0 to 15 do mousebuff[x,y]:=getpixel(t.x+x,t.y+y);
end;
procedure initmouse;
var x,y:word;
begin
freshmouse;
r.ax:=7;r.cx:=0;r.dx:=getmaxx*4;intr($33,r);
r.ax:=8;r.cx:=0;r.dx:=getmaxy*4;intr($33,r);
r.ax:=15;r.cx:=1;r.dx:=1;intr($33,r);
r.ax:=4;r.cx:=(getmaxx div 2)*4;r.dx:=(getmaxy div 2)*4;intr($33,r);
end;
Procedure ClearDevice;
begin
freshmouse;
tgraph.cleardevice;
end;

BEGIN
end.