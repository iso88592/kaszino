(*                                      *)
(*                                                                          *)
(*                                                                          *)
(*                                 KASZIN�                                  *)
(*                                                                          *)
(*                                                                          *)
(*                          2004.02.17-2004.03.30                           *)
(*                                                                          *)
(*                   Neumann J�nos Sz�m�t�stechnikai SZKI                   *)
(*                                                                          *)
(*                       11.c Programoz�s emelt szint                       *)
(*                                                                          *)
(*                                                                          *)
(*  Figyelem! A program csak az iskul�ban futtathat�, h�l�zatba val� beje-  *)
(*                             lentkez�s ut�n!                              *)
(*                                                                          *)
(*                                                                          *)
(*                                                                          *)
(*                                                                          *)
(*                                                                          *)
(*                                      *)
Program Casino;

uses crt,tgraph,umouse,casreg;
var x,y:integer;
c:char;
r1,r2:integer;
Procedure outcharxy2(c:char;x,y:word;a:byte);
const bm:array[0..7]of byte=(128,64,32,16,8,4,2,1);
var y2,x2:word;
k,l,m:byte;
begin
k:=ord(c);
for y2:=0 to 15 do for x2:=0 to 7 do if charbuff[k*16+y2]and bm[x2]=bm[x2]then putpixel(x+x2,y+y2,normalcolor)
else putpixel(x+x2,y+y2,a);
end;
Procedure outtextxy2(x,y:Longint;s:string;a:byte);
var z:byte;
begin
if s<>'' then for z:=1 to ord(s[0])do outcharxy2(s[z],x+z*8-8,y,a);
end;
procedure felsorol;
const nevek:array[0..37]of string[31]=(
'A programot nekik k�sz�nhetitek:','1es csapat(p�ker)','Critical Faliure(baccarat)','3as csapat(black jack)',
'4es csapat(f�lkar� rabl�)','5�s csapat(roulette)','Masters Of Computers(kontroll)','',
'1es csapat:','M�rey P�ter','T�th Antal D�vid','Siska Attila','','Critical Faliure:','Tusor Bal�zs','Jakus Gerg�','Papp Istv�n'
,'','3as csapat:','Pongr�cz Attila','Nagy Tam�s','De�k D�niel','','4es csapat:','Martin Zsolt','Drajk� Attila','Kun Viktor',
'V�gv�lgyi Bal�zs','','5�s csapat:','Paput Zsolt','Horinka G�bor','Stenczinger Attila','','Masters Of Computers:',
'T�th S�gi Kl�ra','Papp Tam�s','V�rkonyi Tibor');
var x,y:longint;
begin
installuserfont('bios');
setcolor(17);
cleardevice;
setcolor(7);
x:=0;
repeat
inc(x);
y:=0;
while (y<38)and(y*16-x<0) do begin
if y*16-x+496>0 then outtextxy2(getmaxx div 2-length(nevek[y])*4,y*16-x+480,nevek[y],17);
inc(y);
end;
until (x=37*16+240)or(port[$60]=1);
end;
function q(x:integer):integer;
begin
if x>1024 then q:=(2048-x) else q:=x;
end;
procedure rajz;
const szov:array[1..9]of string=(
'J�t�kosok...',
'Toplista',
'K�sz�t�k',
'Roulette',
'F�lkar� rabl�',
'Black Jack',
'Baccarat',
'P�ker',
'Kil�p�s');
casino='    C A S I N O    ';
var x,y:integer;
c:char;
r1,r2:integer;
begin
installusershape('cur1.cur');
setcolor(17);
cleardevice;
installuserfont('0916rom.fnt');
setcolor(3);
settextstyle(Balra+Alahuzott);
outtextxy(getmaxx div 2-length(casino)*4+1,20,casino);
outtextxy(getmaxx div 2-length(casino)*4-1,20,casino);
outtextxy(getmaxx div 2-length(casino)*4,21,casino);
outtextxy(getmaxx div 2-length(casino)*4,19,casino);
setcolor(11);
outtextxy(getmaxx div 2-length(casino)*4,20,casino);
setcolor(14);
outtextxy(5,getmaxy-20,'Masters Of Computers');
installuserfont('bios.fnt');
settextstyle(Balra);
freshmouse;
r1:=0;r2:=0;
setrgbpalette(17,0,20,0);
for x:=1 to 9 do begin
setcolor(0);outtextxy(getmaxx div 2-length(szov[x])*4+1,x*20-18+81,szov[x]);
setcolor(17+x);outtextxy(getmaxx div 2-length(szov[x])*4,x*20-18+80,szov[x]);
end;
x:=0;
end;
procedure fomenu;
begin
repeat
rajz;
c:=#0;
repeat
if random(10)=0 then inc(r2);
if r2>1024 then r2:=-1024;
if random(100)=0 then inc(r1);
if r1>255 then r1:=-255;
setrgbpalette(16,63,abs(r1 div 4),0);
for y:=18 to 26 do setrgbpalette(y,abs(q(r2+(y-18)*2) div 32)+31,abs(q(r2+(y-18)*2) div 32)+31,63);
setcolor(17);
if keypressed then repeat c:=readkey;until not keypressed;
if c=#80 then begin rectangle(240,x*20+80,400,x*20+98);x:=x+1;end;
if c=#72 then begin rectangle(240,x*20+80,400,x*20+98);x:=x-1;end;
if x<0 then x:=8;
if x>8 then x:=x-9;
if c<>#13 then c:=#0;
setcolor(16);
rectangle(240,x*20+80,400,x*20+98);
if (t.x>240)and(t.x<400)and(t.y>80)and(t.y<260)and(t.katt)then begin
setcolor(17);rectangle(240,x*20+80,400,x*20+98);x:=(t.y-80)div 20;end;
putmouse;
until ((c=#13)or(t.duplakatt));
if x=2 then felsorol;
until x=8;
end;

BEGIN
randomize;
initgraph(M640x480);
initmouse;
fomenu;
closegraph;
END.